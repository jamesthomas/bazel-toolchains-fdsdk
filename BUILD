load(
    "//toolchain:cc_binary_expressions.bzl",
    _ALL_PATTERN = "ALL_PATTERN",
    _AR_PATTERN = "AR_PATTERN",
    _CPP_PATTERN = "CPP_PATTERN",
    _DWP_PATTERN = "DWP_PATTERN",
    _EXCLUDE_MAN_PATTERN = "EXCLUDE_MAN_PATTERN",
    _EXCLUDE_BUILD_FILE_PATTERN = "EXCLUDE_BUILD_FILE_PATTERN",
    _GCC_PATTERN = "GCC_PATTERN",
    _GCOV_PATTERN = "GCOV_PATTERN",
    _LD_PATTERN = "LD_PATTERN",
    _NM_PATTERN = "NM_PATTERN",
    _OBJCOPY_PATTERN = "OBJCOPY_PATTERN",
    _OBJDUMP_PATTERN = "OBJDUMP_PATTERN",
    _STRIP_PATTERN = "STRIP_PATTERN",
    _LD_LIBRARY_PATHS = "LD_LIBRARY_PATHS",
)

filegroup(
    name = "ld_library_paths",
    srcs = glob(_LD_LIBRARY_PATHS, exclude_directories = 0),
	visibility = ["//visibility:public"],
)

filegroup(
    name = "builtin_include_directories",
    srcs = glob(
        [
            "usr/include",
            "usr/include/linux",
            "usr/lib/gcc",
        ],
        exclude_directories = 0,
    ),
	visibility = ["//visibility:public"],
)

filegroup(
    name = "sysroot_path",
    srcs = glob(
        ["usr/lib/sdk/toolchain-*"],
        exclude_directories = 0,
    ),
    visibility = ["//visibility:public"],
)

_all_binaries_patterns = [
    _AR_PATTERN,
    _CPP_PATTERN,
    _DWP_PATTERN,
    _EXCLUDE_MAN_PATTERN,
    _GCC_PATTERN,
    _GCOV_PATTERN,
    _LD_PATTERN,
    _NM_PATTERN,
    _OBJCOPY_PATTERN,
    _OBJDUMP_PATTERN,
    _STRIP_PATTERN,
]

_exclude_patterns = [
    _EXCLUDE_MAN_PATTERN,
    _EXCLUDE_BUILD_FILE_PATTERN,
]

# TODO: Split dependencies on a per-binary basis
[
    filegroup(
        name = group_name + "_deps",
        srcs = glob(
            [binary],
            allow_empty = False,
        ) + glob(
            [_ALL_PATTERN],
            exclude = _exclude_patterns + _all_binaries_patterns,
            allow_empty = False,
        ),
        visibility = ["//visibility:public"],
    )
    for group_name, binary in [
        ("ar", _AR_PATTERN),
        ("cpp", _CPP_PATTERN),
        ("dwp", _DWP_PATTERN),
        ("gcc", _GCC_PATTERN),
        ("gcov", _GCOV_PATTERN),
        ("ld", _LD_PATTERN),
        ("nm", _NM_PATTERN),
        ("objcopy", _OBJCOPY_PATTERN),
        ("objdump", _OBJDUMP_PATTERN),
        ("strip", _STRIP_PATTERN),
    ]
]

