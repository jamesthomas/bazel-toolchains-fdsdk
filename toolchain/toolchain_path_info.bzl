# Copyright 2019 Codethink Ltd. All rights reserved.
#
# This work is licensed under the terms of the MIT license (see LICENSE).

"""Information used to generate paths for toolchains"""

FdsdkToolchainPathInfo = provider(fields = [
    "ar_path",
    "cpp_path",
    "dwp_path",
    "gcc_path",
    "gcov_path",
    "ld_path",
    "nm_path",
    "objcopy_path",
    "objdump_path",
    "strip_path",
    "builtin_include_directories",
    "sysroot_path",
    "ld_library_paths",
])

def _filegroup_to_path(filegroup):
    return filegroup[DefaultInfo].files.to_list()[0].path

def _filegroup_to_tool_path(filegroup, toolchain_root_path):
    # NOTE: This is only necessary because unnormalized paths don't work with tool_path.
    # The only way to have a normalized path is to only use toolchains in the root repository.
    # There is an open MR to resolve this at https://github.com/bazelbuild/bazel/pull/5809.
    workspace_path = _filegroup_to_path(filegroup)
    return workspace_path.replace(toolchain_root_path + "/", "", 1)

def fdsdk_toolchain_path_info_impl(ctx):
    root_path = ctx.file.anchor_file.dirname
    ar_path = _filegroup_to_tool_path(ctx.attr.ar, root_path)
    cpp_path = _filegroup_to_tool_path(ctx.attr.cpp, root_path)
    dwp_path = _filegroup_to_tool_path(ctx.attr.dwp, root_path)
    gcc_path = _filegroup_to_tool_path(ctx.attr.gcc, root_path)
    gcov_path = _filegroup_to_tool_path(ctx.attr.gcov, root_path)
    ld_path = _filegroup_to_tool_path(ctx.attr.ld, root_path)
    nm_path = _filegroup_to_tool_path(ctx.attr.nm, root_path)
    objcopy_path = _filegroup_to_tool_path(ctx.attr.objcopy, root_path)
    objdump_path = _filegroup_to_tool_path(ctx.attr.objdump, root_path)
    strip_path = _filegroup_to_tool_path(ctx.attr.strip, root_path)

    ld_library_paths = [p.path for p in ctx.attr.ld_library_paths[DefaultInfo].files.to_list()]
    builtin_include_directories = [p.path for p in ctx.attr.builtin_include_directories[DefaultInfo].files.to_list()]
    sysroot_path = ctx.attr.sysroot_path[DefaultInfo].files.to_list()[0].path
    return [FdsdkToolchainPathInfo(
        ar_path = ar_path,
        cpp_path = cpp_path,
        dwp_path = dwp_path,
        gcc_path = gcc_path,
        gcov_path = gcov_path,
        ld_path = ld_path,
        nm_path = nm_path,
        objcopy_path = objcopy_path,
        objdump_path = objdump_path,
        strip_path = strip_path,
        ld_library_paths = ld_library_paths,
        builtin_include_directories = builtin_include_directories,
        sysroot_path = sysroot_path,
    )]

fdsdk_toolchain_path_info = rule(
    implementation = fdsdk_toolchain_path_info_impl,
    attrs = {
        "anchor_file": attr.label(doc = "Label pointing to any file in the toolchain's root directory", allow_single_file = True, mandatory = True),
        "ar": attr.label(doc = "Label pointing to a filegroup that contains the 'ar' binary, then its dependencies", allow_files = True, mandatory = True),
        "cpp": attr.label(doc = "Label pointing to a filegroup that contains the 'cpp' binary, then its dependencies", allow_files = True, mandatory = True),
        "dwp": attr.label(doc = "Label pointing to a filegroup that contains the 'dwp' binary, then its dependencies", allow_files = True, mandatory = True),
        "gcc": attr.label(doc = "Label pointing to a filegroup that contains the 'gcc' binary, then its dependencies", allow_files = True, mandatory = True),
        "gcov": attr.label(doc = "Label pointing to a filegroup that contains the 'gcov' binary, then its dependencies", allow_files = True, mandatory = True),
        "ld": attr.label(doc = "Label pointing to a filegroup that contains the 'ld' binary, then its dependencies", allow_files = True, mandatory = True),
        "nm": attr.label(doc = "Label pointing to a filegroup that contains the 'nm' binary, then its dependencies", allow_files = True, mandatory = True),
        "objcopy": attr.label(doc = "Label pointing to a filegroup that contains the 'objcopy' binary, then its dependencies", allow_files = True, mandatory = True),
        "objdump": attr.label(doc = "Label pointing to a filegroup that contains the 'objdump' binary, then its dependencies", allow_files = True, mandatory = True),
        "strip": attr.label(doc = "Label pointing to a filegroup that contains the 'strip' binary, then its dependencies", allow_files = True, mandatory = True),
        "ld_library_paths": attr.label(doc = "Label pointing to a filegroup that contains the paths to passed in via LD_LIBRARY_PATH", allow_files = True, mandatory = True),
        "builtin_include_directories": attr.label(doc = "Label pointing to a filegroup that contains the built-in include directories", allow_files = True, mandatory = True),
        "sysroot_path": attr.label(doc = "Label pointing to a filegroup that contains the path to the sysroot of the toolchain", allow_files = True, mandatory = True),
    },
    provides = [FdsdkToolchainPathInfo],
)
