Freedesktop SDK toolchains for Bazel
=================

# Running the hello-world demo

* Install BuildStream and Buildstream External
* Build the toolchain:

```
    $ bst build toolchain-tarball-x86.bst
```

* Checkout the toolchain tarball

```
    $ bst checkout toolchain-tarball-x86_64.bst toolchain-tarball-x86_64
```

* The current version of Freedesktop SDK used builds with libc 2.30. If your
  system's libc is older than this you won't be able to build with Bazel. You
  can however create a more up-to-date chroot or container and use Bazel from
  within that, copying in the checked out tarball.

* Edit the WORKSPACE file in 'tests' and change the file:/// url to point to the tarball:

```
    load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
    
    local_repository(
      name = "fdsdk_toolchain_repo",
      path = "..",
    )
    
    load("@fdsdk_toolchain_repo//toolchain:gen_build_defs.bzl", "gen_build_defs")
    
    http_archive(
      name = "fdsdk_archive",
      urls = [
        "file:///src/bazel-toolchains-fdsdk/toolchain-tarball-x86_64/freedesktop-sdk-toolchain-x86_64.tar.xz",
      ],
    )
    
    gen_build_defs(
      name = "fdsdk",
      arch = "x86_64",
      archive = "@fdsdk_archive",
      parent_repo = "@fdsdk_toolchain_repo",
      build_template_file = "@fdsdk_toolchain_repo//toolchain:BUILD.in",
      wrapper_template_file = "@fdsdk_toolchain_repo//toolchain:wrapper.in",
    )

    register_toolchains("@fdsdk//:cc-toolchain")
```

* Add these lines to your ~/.bazelrc file

```
    build:fdsdk --crosstool_top=@fdsdk//:fdsdk
    build:fdsdk --cpu=x86_64
```

* Build the hello-world target in 'tests':

```
    $ bazel build --config=fdsdk //main:hello-world
```

* Run the hello-world executable:

```
    $ bazel run --config=fdsdk //main:hello-world
```

# Building the hello-world demo for an aarch64 target

* Build the toolchain:

```
    $ bst build toolchain-tarball-aarch64.bst
```

* Checkout the toolchain tarball

```
    $ bst checkout toolchain-tarball-aarch64.bst toolchain-tarball-aarch64
```

* Edit the WORKSPACE file in 'tests' and change the file:/// url to point to the tarball:

```
    load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
    
    local_repository(
      name = "fdsdk_toolchain_repo",
      path = "..",
    )
    
    load("@fdsdk_toolchain_repo//toolchain:gen_build_defs.bzl", "gen_build_defs")
    
    http_archive(
      name = "fdsdk_archive",
      urls = [
        "file:///src/bazel-toolchains-fdsdk/toolchain-tarball-aarch64/freedesktop-sdk-toolchain-aarch64.tar.xz",
      ],
    )
    
    gen_build_defs(
      name = "fdsdk",
      arch = "aarch64",
      archive = "@fdsdk_archive",
      parent_repo = "@fdsdk_toolchain_repo",
      build_template_file = "@fdsdk_toolchain_repo//toolchain:BUILD.in",
      wrapper_template_file = "@fdsdk_toolchain_repo//toolchain:wrapper.in",
    )

    register_toolchains("@fdsdk//:cc-toolchain")
```

* Build the hello-world executable

```
    $ bazel build --crosstool_top=@fdsdk//:fdsdk --cpu=aarch64 //main:hello-world
```

# Using the toolchain in other projects

* Follow the instructions in the previous sections, except instead of editing
  the WORKSPACE file with those code blocks, add one of the following code blocks
  to the existing project's WORKSPACE file:

## Rules from the toolchain

i.e. your toolchain binaries and bazel rules come from the exact same place.

```
    load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
    
    http_archive(
      name = "fdsdk_archive",
      urls = [
        "file:///src/bazel-toolchains-fdsdk/toolchain-tarball-x86_64/freedesktop-sdk-toolchain-x86_64.tar.xz",
      ],
    )
    load("@fdsdk_archive//toolchain:gen_build_defs.bzl", "gen_build_defs")
    
    gen_build_defs(
      name = "fdsdk",
      arch = "x86_64",
      archive = "@fdsdk_archive",
      parent_repo = "@fdsdk_archive",
      build_template_file = "@fdsdk_archive//toolchain:BUILD.in",
      wrapper_template_file = "@fdsdk_archive//toolchain:wrapper.in",
    )
    
    register_toolchains("@fdsdk//:cc-toolchain")
```

## Rules from an external repository

i.e. your toolchain binaries come from one place, while the bazel rules come from another.

This is useful for testing changes to the bazel rules without having to
rebuild the entire toolchain.

```
    load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
    load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")
    
    git_repository(
      name = "fdsdk_toolchain_repo",
      branch = "master",
      remote = "https://gitlab.com/CodethinkLabs/bazel-resources/bazel-toolchains-fdsdk",
    )
    
    load("@fdsdk_toolchain_repo//toolchain:gen_build_defs.bzl", "gen_build_defs")
    
    http_archive(
      name = "fdsdk_archive",
      urls = [
        "file:///src/bazel-toolchains-fdsdk/toolchain-tarball-x86_64/freedesktop-sdk-toolchain-x86_64.tar.xz",
      ],
    )
    
    gen_build_defs(
      name = "fdsdk",
      arch = "x86_64",
      archive = "@fdsdk_archive",
      parent_repo = "@fdsdk_toolchain_repo",
      build_template_file = "@fdsdk_toolchain_repo//toolchain:BUILD.in",
      wrapper_template_file = "@fdsdk_toolchain_repo//toolchain:wrapper.in",
    )

    register_toolchains("@fdsdk//:cc-toolchain")
```

##

After installing the WORKSPACE files, use the toolchain by adding the
environment variable `BAZEL_DO_NOT_DETECT_CPP_TOOLCHAIN=1`
(throws up errors if it fails to find the toolchain), and including in your
command-line options `--crosstool_top=@fdsdk//:fdsdk --cpu=x86_64` (or aarch64).

e.g. `BAZEL_DO_NOT_DETECT_CPP_TOOLCHAIN=1 bazel build --crosstool_top=@fdsdk//:fdsdk --cpu=x86_64 //main:...`
